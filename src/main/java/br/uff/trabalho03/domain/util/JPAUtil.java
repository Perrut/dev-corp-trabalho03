package br.uff.trabalho03.domain.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class JPAUtil {

	public static final String PERSISTENCE_UNIT_NAME = "trabalho03PU";
	public static EntityManager entityMgrObj = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME)
			.createEntityManager();
	public static EntityTransaction transactionObj = entityMgrObj.getTransaction();
}

package br.uff.trabalho03.domain.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Query;

import br.uff.trabalho03.domain.entity.Evento;
import br.uff.trabalho03.domain.util.JPAUtil;
import br.uff.trabalho03.exception.EventoNaoEncontradoException;

@SuppressWarnings("unchecked")
public class EventoDAO {
	public static List<Evento> getAllEventos() {
		Query queryObj = JPAUtil.entityMgrObj.createQuery("SELECT e FROM Evento e");
		List<Evento> eventoList = queryObj.getResultList();
		if (eventoList != null && eventoList.size() > 0) {
			return eventoList;
		} else {
			return new ArrayList<>();
		}
	}

	public static Evento createNewEvento(Evento e) {
		if (!JPAUtil.transactionObj.isActive()) {
			JPAUtil.transactionObj.begin();
		}

		JPAUtil.entityMgrObj.persist(e);
		JPAUtil.transactionObj.commit();
		
		return e;
	}

	public static void updateEvento(Evento e) {
		if (!JPAUtil.transactionObj.isActive()) {
			JPAUtil.transactionObj.begin();
		}

		try {
			Evento ev = findEventoOrThrow(e.getId());
			ev.setAreaConcentracao(e.getAreaConcentracao());
			ev.setInstituicaoOrganizadora(e.getInstituicaoOrganizadora());
			ev.setNome(e.getNome());
			ev.setSigla(e.getSigla());
			JPAUtil.entityMgrObj.persist(ev);
			JPAUtil.transactionObj.commit();
		} catch (EventoNaoEncontradoException ex) {
			JPAUtil.transactionObj.rollback();
			throw ex;
		}
	}

	public static Evento getEventoById(Integer eventoId) {
		return findEventoOrThrow(eventoId);
	}

	public static void deleteEvento(Integer eventoId) {
		if (!JPAUtil.transactionObj.isActive()) {
			JPAUtil.transactionObj.begin();
		}

		try {
			Evento deleteEventoObj = findEventoOrThrow(eventoId);
			JPAUtil.entityMgrObj.remove(deleteEventoObj);
		} catch (EventoNaoEncontradoException ex) {
			JPAUtil.transactionObj.rollback();
			throw ex;
		}

		JPAUtil.transactionObj.commit();
	}

	private static Evento findEventoOrThrow(Integer id) {
		Evento evento = JPAUtil.entityMgrObj.find(Evento.class, id);

		if (Objects.isNull(evento)) {
			throw new EventoNaoEncontradoException();
		}

		return evento;
	}
}
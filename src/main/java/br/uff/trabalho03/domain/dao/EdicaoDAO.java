package br.uff.trabalho03.domain.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Query;

import br.uff.trabalho03.domain.entity.Edicao;
import br.uff.trabalho03.domain.entity.Evento;
import br.uff.trabalho03.domain.util.JPAUtil;
import br.uff.trabalho03.exception.EdicaoNaoEncontradaException;
import br.uff.trabalho03.exception.EventoNaoEncontradoException;

@SuppressWarnings("unchecked")
public class EdicaoDAO {
	public static List<Edicao> getAllEdicoesFromEvento(Integer eventoId) {
		try {
			Evento evento = EventoDAO.getEventoById(eventoId);
			Query queryObj = JPAUtil.entityMgrObj.createQuery("SELECT e FROM Edicao e where e.evento = :evento");
			queryObj.setParameter("evento", evento);
			List<Edicao> edicaoList = queryObj.getResultList();

			if (edicaoList != null && edicaoList.size() > 0) {
				return edicaoList;
			} else {
				return new ArrayList<>();
			}
		} catch (EventoNaoEncontradoException ex) {
			throw ex;
		}
	}

	public static Edicao createNewEdicao(Integer eventoId, Edicao e) {
		if (!JPAUtil.transactionObj.isActive()) {
			JPAUtil.transactionObj.begin();
		}
		try {
			Evento evento = EventoDAO.getEventoById(eventoId);
			e.setEvento(evento);
			JPAUtil.entityMgrObj.persist(e);
			JPAUtil.transactionObj.commit();

			return e;
		} catch (EventoNaoEncontradoException ex) {
			throw ex;
		}
	}

	public static void updateEdicao(Integer eventoId, Edicao evento) {
		if (!JPAUtil.transactionObj.isActive()) {
			JPAUtil.transactionObj.begin();
		}

		try {
			Edicao edicao = findEdicaoOrThrow(eventoId, evento.getId());

			edicao.setNumero(evento.getNumero());
			edicao.setAno(evento.getAno());
			edicao.setDataInicio(evento.getDataInicio());
			edicao.setDataFim(evento.getDataFim());
			edicao.setCidade(evento.getCidade());
			edicao.setPais(evento.getPais());
			JPAUtil.entityMgrObj.persist(edicao);

			JPAUtil.transactionObj.commit();
		} catch (EventoNaoEncontradoException | EdicaoNaoEncontradaException ex) {
			JPAUtil.transactionObj.rollback();
			throw ex;
		}
	}

	public static Edicao getEdicaoById(Integer eventoId, Integer edicaoId) {
		try {
			Edicao edicao = findEdicaoOrThrow(eventoId, edicaoId);

			return edicao;
		} catch (EventoNaoEncontradoException | EdicaoNaoEncontradaException ex) {
			throw ex;
		}
	}

	public static void deleteEdicao(Integer eventoId, Integer edicaoId) {
		if (!JPAUtil.transactionObj.isActive()) {
			JPAUtil.transactionObj.begin();
		}

		try {
			Edicao deleteEdicaoObj = findEdicaoOrThrow(eventoId, edicaoId);
			deleteEdicaoObj.setId(edicaoId);
			JPAUtil.entityMgrObj.remove(JPAUtil.entityMgrObj.merge(deleteEdicaoObj));
			JPAUtil.transactionObj.commit();
		} catch (EventoNaoEncontradoException | EdicaoNaoEncontradaException ex) {
			JPAUtil.transactionObj.rollback();
			throw ex;
		}
	}

	public static Map<Evento, List<Edicao>> findEdicaoByDate(Date dataInicio, Date dataFim) {
		try {
			String queryString = "SELECT e FROM Edicao e where e.dataInicio >= :dataInicio";
			queryString = Objects.nonNull(dataFim) ? queryString + " and (e.dataInicio <= :dataFim or e.dataFim IS NULL)"
					: queryString;

			Query queryObj = JPAUtil.entityMgrObj.createQuery(queryString);
			queryObj.setParameter("dataInicio", dataInicio);
			if (Objects.nonNull(dataFim)) {
				queryObj.setParameter("dataFim", dataFim);
			}

			List<Edicao> edicaoList = queryObj.getResultList();

			if (edicaoList != null && edicaoList.size() > 0) {
				Set<Evento> eventos = new HashSet<>();
				edicaoList.forEach(ed -> eventos.add(ed.getEvento()));
				
				Map<Evento, List<Edicao>> eventoComEdicoes = new HashMap<>();
				eventos.forEach(e -> eventoComEdicoes.put(e, new ArrayList<>()));
				edicaoList.forEach(ed -> eventoComEdicoes.get(ed.getEvento()).add(ed));

				return eventoComEdicoes;
			} else {
				return new HashMap<>();
			}
		} catch (EventoNaoEncontradoException ex) {
			throw ex;
		}
	}

	public static Map<Evento, List<Edicao>> findEdicaoByCidade(String cidade) {
		try {
			String queryString = "SELECT e FROM Edicao e where upper(e.cidade) like upper(:cidade)";

			Query queryObj = JPAUtil.entityMgrObj.createQuery(queryString);
			queryObj.setParameter("cidade", cidade);

			List<Edicao> edicaoList = queryObj.getResultList();

			if (edicaoList != null && edicaoList.size() > 0) {
				Set<Evento> eventos = new HashSet<>();
				edicaoList.forEach(ed -> eventos.add(ed.getEvento()));
				
				Map<Evento, List<Edicao>> eventoComEdicoes = new HashMap<>();
				eventos.forEach(e -> eventoComEdicoes.put(e, new ArrayList<>()));
				edicaoList.forEach(ed -> eventoComEdicoes.get(ed.getEvento()).add(ed));

				return eventoComEdicoes;
			} else {
				return new HashMap<>();
			}
		} catch (EventoNaoEncontradoException ex) {
			throw ex;
		}
	}

	public static Map<Evento, List<Edicao>> findEdicaoByCidadeEData(String cidade, Date dataInicio, Date dataFim) {
		try {
			String queryString = "SELECT e FROM Edicao e where upper(e.cidade) like upper(:cidade)"
					+ " and e.dataInicio >= :dataInicio";
			queryString = Objects.nonNull(dataFim) ? queryString + " and (e.dataInicio <= :dataFim or e.dataFim IS NULL)"
					: queryString;

			Query queryObj = JPAUtil.entityMgrObj.createQuery(queryString);
			queryObj.setParameter("cidade", cidade);
			queryObj.setParameter("dataInicio", dataInicio);
			if (Objects.nonNull(dataFim)) {
				queryObj.setParameter("dataFim", dataFim);
			}

			List<Edicao> edicaoList = queryObj.getResultList();

			if (edicaoList != null && edicaoList.size() > 0) {
				Set<Evento> eventos = new HashSet<>();
				edicaoList.forEach(ed -> eventos.add(ed.getEvento()));
				
				Map<Evento, List<Edicao>> eventoComEdicoes = new HashMap<>();
				eventos.forEach(e -> eventoComEdicoes.put(e, new ArrayList<>()));
				edicaoList.forEach(ed -> eventoComEdicoes.get(ed.getEvento()).add(ed));

				return eventoComEdicoes;
			} else {
				return new HashMap<>();
			}
		} catch (EventoNaoEncontradoException ex) {
			throw ex;
		}
	}

	private static Edicao findEdicaoOrThrow(Integer eventoId, Integer edicaoId) {
		try {
			Evento evento = EventoDAO.getEventoById(eventoId);
			Query queryObj = JPAUtil.entityMgrObj
					.createQuery("SELECT e FROM Edicao e where e.evento = :evento and e.id = :id");
			queryObj.setParameter("evento", evento);
			queryObj.setParameter("id", edicaoId);
			Edicao edicao = (Edicao) queryObj.getSingleResult();
			if (edicao == null) {
				throw new EdicaoNaoEncontradaException();
			}
			return edicao;
		} catch (EventoNaoEncontradoException ex) {
			throw ex;
		}
	}
}
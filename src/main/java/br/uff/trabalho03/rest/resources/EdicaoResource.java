package br.uff.trabalho03.rest.resources;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;

import br.uff.trabalho03.domain.dao.EdicaoDAO;
import br.uff.trabalho03.domain.entity.Edicao;
import br.uff.trabalho03.domain.entity.Evento;
import br.uff.trabalho03.exception.EdicaoNaoEncontradaException;
import br.uff.trabalho03.exception.EventoNaoEncontradoException;
import br.uff.trabalho03.rest.dto.EdicaoDTO;
import br.uff.trabalho03.rest.dto.EventoComEdicaoDTO;

@Path("eventos")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class EdicaoResource {
	private final Gson gson = new Gson();

	@GET
	@Path("{eventoId}/edicoes")
	public Response getAllEdicoesFromEvento(@PathParam("eventoId") Integer eventoId) {
		try {
			List<Edicao> edicoes = EdicaoDAO.getAllEdicoesFromEvento(eventoId);
			List<EdicaoDTO> dtos = edicoes.stream().map(EdicaoDTO::generateFromEdicao).collect(Collectors.toList());

			return Response.ok().entity(gson.toJson(dtos)).build();
		} catch (EventoNaoEncontradoException ex) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("").build();
		}
	}

	@POST
	@Path("{eventoId}/edicoes")
	public Response createNewEdicao(@PathParam("eventoId") Integer eventoId, String edicao) {
		try {
			Edicao novaEdicao = gson.fromJson(edicao, Edicao.class);
			Edicao ed = EdicaoDAO.createNewEdicao(eventoId, novaEdicao);

			return Response.status(Status.CREATED).entity(gson.toJson(EdicaoDTO.generateFromEdicao(ed))).build();
		} catch (EventoNaoEncontradoException ex) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("").build();
		}
	}

	@PUT
	@Path("{eventoId}/edicoes/{id}")
	public Response updateEdicao(@PathParam("eventoId") Integer eventoId, @PathParam("id") Integer id, String edicao) {
		try {
			Edicao edicaoAtualizada = gson.fromJson(edicao, Edicao.class);
			edicaoAtualizada.setId(id);
			EdicaoDAO.updateEdicao(eventoId, edicaoAtualizada);

			return Response.noContent().build();
		} catch (EventoNaoEncontradoException | EdicaoNaoEncontradaException ex) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("").build();
		}
	}

	@GET
	@Path("{eventoId}/edicoes/{id}")
	public Response getEdicaoById(@PathParam("eventoId") Integer eventoId, @PathParam("id") Integer id) {
		try {
			Edicao edicao = EdicaoDAO.getEdicaoById(eventoId, id);
			EdicaoDTO dto = EdicaoDTO.generateFromEdicao(edicao);

			return Response.ok().entity(gson.toJson(dto)).build();
		} catch (EventoNaoEncontradoException | EdicaoNaoEncontradaException ex) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("").build();
		}
	}

	@DELETE
	@Path("{eventoId}/edicoes/{id}")
	public Response deleteEdicao(@PathParam("eventoId") Integer eventoId, @PathParam("id") Integer id) {
		try {
			EdicaoDAO.deleteEdicao(eventoId, id);

			return Response.noContent().build();
		} catch (EventoNaoEncontradoException | EdicaoNaoEncontradaException ex) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity("").build();
		}
	}

	@GET
	@Path("edicoes/data")
	public Response findEdicaoByDate(@QueryParam("dataInicio") String dataInicio,
			@QueryParam("dataFim") String dataFim) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		try {
			Date convertedDataInicio = sdf.parse(dataInicio);
			Date convertedDataFim = Objects.nonNull(dataFim) ? sdf.parse(dataFim) : null;
			Map<Evento, List<Edicao>> eventos = EdicaoDAO.findEdicaoByDate(convertedDataInicio, convertedDataFim);

			List<EventoComEdicaoDTO> dtos = eventos.keySet().stream().map(ev -> {
				return EventoComEdicaoDTO.generateFromEvento(ev, eventos.get(ev));
			}).collect(Collectors.toList());

			return Response.ok().entity(gson.toJson(dtos)).build();
		} catch (ParseException e) {
			return Response.status(Status.BAD_REQUEST).entity("").build();
		} catch (EventoNaoEncontradoException ex) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		}
	}

	@GET
	@Path("edicoes/cidade")
	public Response findEdicaoByCidade(@QueryParam("cidade") String cidade) {
		try {
			Map<Evento, List<Edicao>> eventos = EdicaoDAO.findEdicaoByCidade(cidade);

			List<EventoComEdicaoDTO> dtos = eventos.keySet().stream().map(ev -> {
				return EventoComEdicaoDTO.generateFromEvento(ev, eventos.get(ev));
			}).collect(Collectors.toList());

			return Response.ok().entity(gson.toJson(dtos)).build();
		} catch (EventoNaoEncontradoException ex) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		}
	}

	@GET
	@Path("edicoes/cidade-e-data")
	public Response findEdicaoByCidadeEData(@QueryParam("cidade") String cidade,
			@QueryParam("dataInicio") String dataInicio, @QueryParam("dataFim") String dataFim) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		try {
			Date convertedDataInicio = sdf.parse(dataInicio);
			Date convertedDataFim = Objects.nonNull(dataFim) ? sdf.parse(dataFim) : null;
			Map<Evento, List<Edicao>> eventos = EdicaoDAO.findEdicaoByCidadeEData(cidade, convertedDataInicio,
					convertedDataFim);

			List<EventoComEdicaoDTO> dtos = eventos.keySet().stream().map(ev -> {
				return EventoComEdicaoDTO.generateFromEvento(ev, eventos.get(ev));
			}).collect(Collectors.toList());

			return Response.ok().entity(gson.toJson(dtos)).build();
		} catch (ParseException e) {
			return Response.status(Status.BAD_REQUEST).entity("").build();
		} catch (EventoNaoEncontradoException ex) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		}
	}
}

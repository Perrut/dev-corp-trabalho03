package br.uff.trabalho03.rest.resources;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;

import br.uff.trabalho03.domain.dao.EventoDAO;
import br.uff.trabalho03.domain.entity.Evento;
import br.uff.trabalho03.exception.EventoNaoEncontradoException;
import br.uff.trabalho03.rest.dto.EventoDTO;

@Path("eventos")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class EventoResource {

	private final Gson gson = new Gson();

	@GET
	public String getAllEventos() {
		List<Evento> eventos = EventoDAO.getAllEventos();
		List<EventoDTO> eventosDto = eventos.stream().map(EventoDTO::generateFromEvento).collect(Collectors.toList());

		return gson.toJson(eventosDto);
	}

	@POST
	public String createNewEvento(String evento) {
		Evento novoEvento = gson.fromJson(evento, Evento.class);
		return gson.toJson(EventoDTO.generateFromEvento(EventoDAO.createNewEvento(novoEvento)));
	}

	@PUT
	@Path("{id}")
	public Response updateEvento(String evento, @PathParam("id") Integer id) {
		Evento eventoAtualizado = gson.fromJson(evento, Evento.class);
		eventoAtualizado.setId(id);
		try {
			EventoDAO.updateEvento(eventoAtualizado);
			return Response.noContent().build();
		} catch (EventoNaoEncontradoException e) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GET
	@Path("{id}")
	public Response getEventoById(@PathParam("id") Integer id) {
		try {
			Evento evento = EventoDAO.getEventoById(id);
			EventoDTO dto = EventoDTO.generateFromEvento(evento);
			return Response.ok().entity(gson.toJson(dto)).build();
		} catch (EventoNaoEncontradoException e) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}

	@DELETE
	@Path("{id}")
	public Response deleteEvento(@PathParam("id") Integer id) {
		try {
			EventoDAO.deleteEvento(id);
			return Response.noContent().build();
		} catch (EventoNaoEncontradoException e) {
			return Response.status(Status.NOT_FOUND).entity("").build();
		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}
	}
}

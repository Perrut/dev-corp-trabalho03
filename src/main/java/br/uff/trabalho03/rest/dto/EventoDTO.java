package br.uff.trabalho03.rest.dto;

import br.uff.trabalho03.domain.entity.Evento;

public class EventoDTO {
	private Integer id;
	private String nome;
	private String sigla;
	private String areaConcentracao;
	private String instituicaoOrganizadora;

	public static EventoDTO generateFromEvento(Evento evento) {
		EventoDTO dto = new EventoDTO();

		dto.setAreaConcentracao(evento.getAreaConcentracao());
		dto.setId(evento.getId());
		dto.setInstituicaoOrganizadora(evento.getInstituicaoOrganizadora());
		dto.setNome(evento.getNome());
		dto.setSigla(evento.getSigla());

		return dto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getAreaConcentracao() {
		return areaConcentracao;
	}

	public void setAreaConcentracao(String areaConcentracao) {
		this.areaConcentracao = areaConcentracao;
	}

	public String getInstituicaoOrganizadora() {
		return instituicaoOrganizadora;
	}

	public void setInstituicaoOrganizadora(String instituicaoOrganizadora) {
		this.instituicaoOrganizadora = instituicaoOrganizadora;
	}
}

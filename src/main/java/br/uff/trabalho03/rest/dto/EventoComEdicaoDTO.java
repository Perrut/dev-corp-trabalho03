package br.uff.trabalho03.rest.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.uff.trabalho03.domain.entity.Edicao;
import br.uff.trabalho03.domain.entity.Evento;

public class EventoComEdicaoDTO {

	private EventoDTO evento;
	private List<EdicaoDTO> edicoes = new ArrayList<>();;

	public static EventoComEdicaoDTO generateFromEvento(Evento evento, List<Edicao> edicoes) {
		EventoComEdicaoDTO dto = new EventoComEdicaoDTO();
		dto.evento = EventoDTO.generateFromEvento(evento);
		dto.edicoes = edicoes.stream().map(EdicaoDTO::generateFromEdicao).collect(Collectors.toList());

		return dto;
	}

	public EventoDTO getEvento() {
		return evento;
	}

	public void setEvento(EventoDTO evento) {
		this.evento = evento;
	}

	public List<EdicaoDTO> getEdicoes() {
		return edicoes;
	}

	public void setEdicoes(List<EdicaoDTO> edicoes) {
		this.edicoes = edicoes;
	}

}
package br.uff.trabalho03.rest.dto;

import java.text.SimpleDateFormat;
import java.util.Objects;

import br.uff.trabalho03.domain.entity.Edicao;

public class EdicaoDTO {
	private Integer id;
	private Integer numero;
	private Integer ano;
	private String dataInicio;
	private String dataFim;
	private String cidade;
	private String pais;

	public static EdicaoDTO generateFromEdicao(Edicao edicao) {
		EdicaoDTO dto = new EdicaoDTO();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		dto.setAno(edicao.getAno());
		dto.setCidade(edicao.getCidade());
		if (Objects.nonNull(edicao.getDataFim())) {
			dto.setDataFim(sdf.format(edicao.getDataFim()));
		}
		dto.setDataInicio(sdf.format(edicao.getDataInicio()));
		dto.setId(edicao.getId());
		dto.setNumero(edicao.getNumero());
		dto.setPais(edicao.getPais());

		return dto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}
}

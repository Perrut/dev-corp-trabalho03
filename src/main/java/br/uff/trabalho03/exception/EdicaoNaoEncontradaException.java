package br.uff.trabalho03.exception;

public class EdicaoNaoEncontradaException extends RuntimeException {
	private static final long serialVersionUID = -1919550418022759377L;

	public EdicaoNaoEncontradaException() {
		super();
	}
}

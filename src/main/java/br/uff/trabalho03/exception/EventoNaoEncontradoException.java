package br.uff.trabalho03.exception;

public class EventoNaoEncontradoException extends RuntimeException {
	private static final long serialVersionUID = -4814180442207917260L;

	public EventoNaoEncontradoException() {
		super();
	}
}
